## European Council Voting Records
Author: Quentin BISET  
Version: 1.0.0  
Platform: Android+iOS via Flutter  
Design pattern: BLoC  
Language: Dart  
Packages: flutter_bloc, equatable, rxdart (to debounce events in the BLoC), http, intl (for date formatting).  

---
### What does the app do?
The app uses a [free web API ](http://api.epdb.eu/) to obtain the voting records of all proposals considered by the European Council,
 and displays them in a **UISplitViewController**.

---
### How does it work?
###### UI layer
This FLutter project is a Material App consisting of 2 main views:
1. **ProposalsListPage** displays a list of all proposals voted on by the European Council;
2. **ProposalDetailPage** displays the detail of each country's vote for a particular proposal.

When the user taps on a proposal in the list, the app navigates to the detail page for said proposal.

While the data is very unlikely to change, we wanted to give the user a way to query the webservice again (beyond killing and restarting the app)
 if the first query failed for whatever reason: as a result, a refresh button can be found on **ProposalsListPage**'s AppBar.
 
###### BLoC layer
**ProposalsBloc** is responsible for getting the proposals data when a fetch event is thrown (either automatically at first launch,
 or manually when the user taps the refresh button), and preparing and sending it to **ProposalsListPage** through a **ProposalsState** object.
 
###### Repository/Data layer
A **ProposalsRepository** is used to obtain the proposals data. The repository works as follow:
1. An instance of **CountriesWebservice** fetches the ID, name and voting weight of all EU countries from [this webservice](http://api.epdb.eu/council/country/);
2. An instance of **ProposalsWebservice** fetches all relevant proposals data from [this webservice](http://api.epdb.eu/council/document/);
3. The results of both requests are aggregated and transformed into proper model objects (**Proposal**, **Vote**, **Country**).