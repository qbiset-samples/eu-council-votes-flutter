/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:eu_council_votes/models/country.dart';

/// {@category Repository}
/// {@subCategory Data}
/// A service class used to retrieve country data from the relevant web API.
class CountriesWebservice {
  // Properties ---------------------------------------------
  /// The URL of the webservice.
  final String url = "http://api.epdb.eu/council/country/";

  // Constructors -------------------------------------------
  /// Default constructor.
  const CountriesWebservice();

  // Public functions ---------------------------------------
  /// Queries the webservice to get the list of countries voting on the European Council.
  ///
  /// There is no need to transform the data for the UI so the function can directly return [Country] objects.
  Future<Set<Country>> getCountries() async {
    final response = await http.get(url);

    if (response.statusCode != 200) {
      throw Exception("Failed to fetch countries from API (HTTP code ${response.statusCode}).");
    }

    try {
      final data = json.decode(response.body) as Map<String, dynamic>;
      // We only care about the values here.
      final iterator = data.values.cast<Map<String, dynamic>>();
      return iterator.map((rawCountry) => Country.fromJSON(rawCountry)).toSet();
    }
    on Exception {
      throw Exception("Failed to parse JSON data into Country objects.");
    }
  }
}
