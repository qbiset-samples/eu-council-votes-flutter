/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;

import 'package:eu_council_votes/models/proposal.dart';
import 'package:eu_council_votes/models/vote.dart';

/// {@category Repository}
/// {@subCategory Data}
/// A service class used to retrieve proposals data from the relevant web API.
class ProposalsWebservice {
  // Properties ---------------------------------------------
  /// The URL of the webservice.
  final String url = "http://api.epdb.eu/council/document/";

  // Constructors -------------------------------------------
  /// Default constructor.
  const ProposalsWebservice();

  // Public functions ---------------------------------------
  /// Queries the webservice to get the list of proposals voted on by the European Council.
  ///
  /// The [RawProposal] objects returned by this function need to be transformed into [Proposal] objects
  /// by the repository before the data can be displayed by the UI.
  Future<Set<RawProposal>> getProposals() async {
    final response = await http.get(url);

    if (response.statusCode != 200) {
      throw Exception("Failed to fetch proposals from API (HTTP code ${response.statusCode}).");
    }

    try {
      final data = json.decode(response.body) as Map<String, dynamic>;
      // We only care about the values here.
      final iterator = data.values.cast<Map<String, dynamic>>();
      return iterator.map((jsonProposal) => RawProposal.fromJSON(jsonProposal)).toSet();
    } on Exception {
      throw Exception("Failed to parse JSON data into RawProposal objects.");
    }
  }
}

/// {@category Repository}
/// {@subCategory Data}
/// A class containing the data for a [Proposal], as received from [ProposalsWebservice].
class RawProposal {
  // Properties ---------------------------------------------
  /// An arbitrary identification number.
  final String identifier;

  /// The full title of the proposal.
  ///
  /// The data is dirty and needs to be trimmed before display.
  final String title;

  /// The date the proposal was voted on, in YYYY-MM-DD format.
  final String dateString;

  /// The raw votes data.
  final Set<RawVote> votes;

  // Constructors -------------------------------------------
  /// Default constructor.
  RawProposal(this.identifier, this.title, this.dateString, this.votes);

  /// Creates an instance of [RawProposal] from a JSON object.
  factory RawProposal.fromJSON(Map<String, dynamic> json) {
    String identifier = json['vote_id'];
    String title = json['vote_title'];
    String dateString = json['date'];

    final jsonVotes = (json['votes'] as List<dynamic>).cast<Map<String, dynamic>>();
    Set<RawVote> rawVotes = jsonVotes.map((jsonVote) => RawVote.fromJSON(jsonVote)).toSet();

    return RawProposal(identifier, title, dateString, rawVotes);
  }
}

/// {@category Repository}
/// {@subCategory Data}
/// A class containing the data for a [Vote], as received from [ProposalsWebservice].
///
/// The countries data is retrieved from a different webservice so we only have an identifier
/// until both data sets can be aggregated.
class RawVote {
  // Properties ---------------------------------------------
  /// An arbitrary identification number for the country voting.
  final String countryID;

  /// The value of the vote.
  final String value;

  // Constructors -------------------------------------------
  /// Default constructor.
  RawVote(this.countryID, this.value);

  /// Creates an instance of [RawVote] from a JSON object.
  factory RawVote.fromJSON(Map<String, dynamic> json) {
    String countryID = json['country_id'];
    String value = json['vote'];
    return RawVote(countryID, value);
  }
}
