/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'dart:async';

import 'package:eu_council_votes/blocs/proposals_bloc.dart';
import 'package:eu_council_votes/repositories/data/data.dart';
import 'package:eu_council_votes/models/models.dart';

/// {@category Repository}
/// {@subCategory Repository}
/// The repository in charge of producing proposals data for [ProposalsBloc].
///
/// It makes use of [CountriesWebservice] and [ProposalsWebservice] to obtain the raw data,
/// then aggregate them into a definitive List of [Proposal] objects.
class ProposalsRepository {
  // Constructors -------------------------------------------
  /// Default constructor.
  const ProposalsRepository();

  // Public functions ---------------------------------------
  /// Queries the web API asynchronously for the list of proposals.
  ///
  /// If an exception is raised from the Data layer, it is passed up the chain.
  Future<List<Proposal>> getProposals() async {
    try {
      CountriesWebservice countriesWebservice = CountriesWebservice();
      Set<Country> countries = await countriesWebservice.getCountries();

      ProposalsWebservice proposalsWebservice = ProposalsWebservice();
      Set<RawProposal> rawProposals = await proposalsWebservice.getProposals();

      return _transformRawObjects(rawProposals, countries);
    }
    on Exception catch(e) {
      throw e;
    }
  }

  // Private functions --------------------------------------
  /// Transforms the raw objects received from the data layer into the definitive list of proposals,
  /// sorted chronologically.
  ///
  /// In particular the title needs to be trimmed and the votes need to be linked to their corresponding country.
  List<Proposal> _transformRawObjects(Set<RawProposal> rawProposals, Set<Country> countries) {
    List<Proposal> proposals = [];

    for (var rawProposal in rawProposals) {
      String identifier = rawProposal.identifier;
      // The data received from the API is unclean, so we need to clean up the title a bit.
      String title = rawProposal.title.trim();
      DateTime date = DateTime.parse(rawProposal.dateString);
      List<Vote> votes = _aggregateVotesAndCountries(rawProposal.votes, countries);

      proposals.add(Proposal(identifier, title, date, votes));
    }
    // Sorting proposals chronologically.
    proposals.sort((proposal1, proposal2) => proposal1.date.compareTo(proposal2.date));

    return proposals;
  }

  /// Aggregates the raw data into a definitive list of [Vote] objects, sorted by country name.
  ///
  /// Since we receive the country and vote data separately, we have to aggregate [rawVotes] and
  /// [countries] manually using the country's String identifier.
  List<Vote> _aggregateVotesAndCountries(Set<RawVote> rawVotes, Set<Country> countries) {
    List<Vote> votes = [];

    for (var rawVote in rawVotes) {
      Country country = countries.singleWhere((element) => element.identifier == rawVote.countryID);
      String value = rawVote.value;

      votes.add(Vote(country, value));
    }
    // Sorting votes by country name, in alphabetic order.
    votes.sort((vote1, vote2) => vote1.country.name.compareTo(vote2.country.name));

    return votes;
  }
}