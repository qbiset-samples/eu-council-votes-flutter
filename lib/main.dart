/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:eu_council_votes/ui/pages/proposals_list_page.dart';
import 'package:eu_council_votes/blocs/proposals_bloc.dart';

/// {@category App}
void main() {
  runApp(EuVotesApp());
}

/// {@category App}
/// The root of the widget tree.
class EuVotesApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ProposalsBloc(),
      child: defaultApp(),
      lazy: false,
    );
  }

  MaterialApp defaultApp() {
    return MaterialApp(
      title: "European Council Votes",
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: ProposalsListPage(),
    );
  }
}
