/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

export './pages/proposals_list_page.dart';
export './widgets/proposals_list_view.dart';