/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'package:flutter/material.dart';
import 'package:eu_council_votes/models/proposal.dart';
import 'package:eu_council_votes/models/vote.dart';
import 'package:eu_council_votes/ui/pages/proposal_detail_page.dart';

/// {@category UI}
/// {@subCategory Proposal Detail}
/// A Widget displaying a [Proposal]'s date as part of a [ProposalDetailPage].
class DateWidget extends StatelessWidget {
  // Properties ---------------------------------------------
  /// String representation of the proposal's date.
  final String dateString;

  // Constructors -------------------------------------------
  /// Default constructor.
  const DateWidget({Key key, this.dateString}) : super(key: key);

  // Widget methods -----------------------------------------
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0, bottom: 5.0),
        child: Text(dateString, style: Theme.of(context).textTheme.bodyText2));
  }
}

/// {@category UI}
/// {@subCategory Proposal Detail}
/// A Widget displaying a [Proposal]'s title as part of a [ProposalDetailPage].
class TitleWidget extends StatelessWidget {
  // Properties ---------------------------------------------
  /// The title of the proposal.
  final String title;

  // Constructors -------------------------------------------
  /// Default constructor.
  const TitleWidget({Key key, this.title}) : super(key: key);

  // Widget methods -----------------------------------------
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 10.0),
        child: Text(title, style: Theme.of(context).textTheme.bodyText1, textAlign: TextAlign.justify));
  }
}

/// {@category UI}
/// {@subCategory Proposal Detail}
/// A Widget displaying a single [Vote]'s country and value.
class VoteTile extends StatelessWidget {
  // Properties ---------------------------------------------
  /// The vote whose data will be displayed.
  final Vote vote;

  // Constructors -------------------------------------------
  /// Default constructor.
  const VoteTile({Key key, this.vote}) : super(key: key);

  // Widget methods -----------------------------------------
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
      child: Row(children: [
        Text(vote.country.name, style: Theme.of(context).textTheme.bodyText2),
        Spacer(),
        Text(vote.value, style: Theme.of(context).textTheme.bodyText2)
      ]),
    );
  }
}