/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'package:flutter/material.dart';
import 'package:eu_council_votes/models/proposal.dart';

/// {@category UI}
/// {@subCategory Proposals List}
/// A Widget displaying a ListView designed to show a list of proposals.
class ProposalsListView extends StatelessWidget {
  // Properties ---------------------------------------------
  /// The list of proposals to display.
  final List<Proposal> proposals;

  /// The function that will be called when the user taps a tile.
  final void Function(Proposal proposal) onTileTap;

  // Constructors -------------------------------------------
  /// Default constructor.
  const ProposalsListView({Key key, this.proposals, this.onTileTap}) : super(key: key);

  // Widget methods -----------------------------------------
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemBuilder: (BuildContext context, int index) {
        Proposal proposal = proposals[index];
        return ProposalsListTile(
          key: ValueKey(proposal.identifier),
          proposal: proposal,
          onTap: () => onTileTap(proposal),
        );
      },
      itemCount: proposals.length,
      separatorBuilder: (context, index) => const Divider(height: 1),
    );
  }
}

/// {@category UI}
/// {@subCategory Proposals List}
/// A Widget displaying a ListTile, designed to show a [Proposal]'s data.
class ProposalsListTile extends StatelessWidget {
  // Properties ---------------------------------------------
  /// The proposal whose data the tile will display.
  final Proposal proposal;

  /// The function that will be called when the user taps the tile.
  final void Function() onTap;

  // Constructors -------------------------------------------
  /// Default constructor.
  const ProposalsListTile({Key key, @required this.proposal, this.onTap}) : super(key: key);

  // Widget methods -----------------------------------------
  @override
  Widget build(BuildContext context) {
    return ListTile(
      isThreeLine: true,
      title: Text(proposal.dateString, maxLines: 1, style: Theme.of(context).textTheme.headline6),
      subtitle: Text(proposal.title,
          maxLines: 2, overflow: TextOverflow.ellipsis, style: Theme.of(context).textTheme.subtitle1),
      trailing: const Center(
        child: Icon(Icons.arrow_forward_ios),
        widthFactor: 1,
      ),
      onTap: onTap,
    );
  }
}