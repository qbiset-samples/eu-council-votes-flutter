/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

export './pages/proposal_detail_page.dart';
export './widgets/proposal_detail_widgets.dart';