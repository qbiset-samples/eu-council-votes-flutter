/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'package:flutter/material.dart';
import 'package:eu_council_votes/models/proposal.dart';
import 'package:eu_council_votes/ui/pages/proposals_list_page.dart';
import 'package:eu_council_votes/ui/widgets/proposal_detail_widgets.dart';

/// {@category UI}
/// {@subCategory Proposal Detail}
/// This page displays all the data of a [Proposal].
///
/// When the user taps a proposal in [ProposalsListPage], we navigate to this page where the date, full title
/// and the list of each country's vote will be displayed.
class ProposalDetailPage extends StatelessWidget {
  // Properties ---------------------------------------------
  /// The proposal whose data will be displayed.
  final Proposal proposal;

  // Constructors -------------------------------------------
  /// Default constructor.
  const ProposalDetailPage({Key key, this.proposal}) : super(key: key);

  // Widget methods -----------------------------------------
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("European Council Votes"), centerTitle: true),
        body: ListView.separated(
            itemBuilder: (context, index) {
              switch (index) {
                case 0:
                  {
                    return DateWidget(key: ValueKey("DateLabel"), dateString: proposal.dateString);
                  }
                  break;

                case 1:
                  {
                    return TitleWidget(key: ValueKey("TitleLabel"), title: proposal.title);
                  }
                  break;

                default:
                  {
                    final vote = proposal.votes[index - 2];
                    return VoteTile(key: ValueKey(vote.country.identifier), vote: vote);
                  }
                  break;
              }
            },
            itemCount: proposal.votes.length + 2, // Date, title, votes
            separatorBuilder: (context, index) {
              if (index > 0) {
                return const Divider(height: 1);
              } else {
                return const Divider(height: 1, color: Colors.transparent);
              }
            }));
  }
}