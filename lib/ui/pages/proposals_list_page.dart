/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:eu_council_votes/blocs/blocs.dart';

import 'package:eu_council_votes/models/proposal.dart';

import 'package:eu_council_votes/ui/widgets/proposals_list_view.dart';
import 'package:eu_council_votes/ui/pages/proposal_detail_page.dart';

/// {@category UI}
/// {@subCategory Proposals List}
/// The first page displayed when launching the app.
///
/// It displays the complete list of all proposals voted on by the European Council
/// through a [ProposalsListView] widget, a refresh indicator when the data is being fetched from the web API,
/// and a SnackBar if the request fails.
///
/// Its content is hooked up to a [ProposalsBloc] that will be query the web API and update the state
/// when a fetch event is thrown. This happens automatically when the page first appears,
/// or manually when the user taps a refresh button in the AppBar.
///
/// If the user taps an entry in the list, we navigate to a [ProposalDetailPage].
class ProposalsListPage extends StatefulWidget {
  @override
  _ProposalsListPageState createState() => _ProposalsListPageState();
}

/// {@category UI}
/// {@subCategory Proposals List}
/// The state of [ProposalsListPage].
///
/// Its main responsibility is to send fetch events to [ProposalsBloc].
class _ProposalsListPageState extends State<ProposalsListPage> {
  // Properties ---------------------------------------------
  /// The BLoC associated with the page.
  ProposalsBloc _proposalsBloc;

  // State methods ------------------------------------------
  @override
  void initState() {
    super.initState();
    _proposalsBloc = BlocProvider.of<ProposalsBloc>(context);

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _proposalsBloc.add(ProposalsEvent.fetch);
    });
  }

  // Widget methods -----------------------------------------
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("European Council Votes"),
          centerTitle: true,
          actions: [
            IconButton(
                icon: const Icon(Icons.refresh),
                color: Colors.white,
                highlightColor: Colors.grey,
                onPressed: () {
                  _proposalsBloc.add(ProposalsEvent.fetch);
                })
          ],
        ),
        body: BlocConsumer<ProposalsBloc, ProposalsState>(
          builder: (context, state) {
            final proposalsListView = ProposalsListView(
                key: ValueKey("ProposalsListView"),
                proposals: state.proposals,
                onTileTap: (Proposal proposal) {
                  final route = MaterialPageRoute(builder: (context) => ProposalDetailPage(proposal: proposal));
                  Navigator.push(context, route);
                });

            List<Widget> widgets = [proposalsListView];

            if (state.showRefreshIndicator) {
              final refreshIndicator =
                  const CircularProgressIndicator(key: ValueKey("RefreshIndicator"), backgroundColor: Colors.grey);
              widgets.add(refreshIndicator);
            }

            return Stack(children: widgets, alignment: Alignment.center);
          },
          listener: (context, state) {
            if (state is ProposalsError) {
              final snackBar = _buildSnackBar(text: state.errorMessage);
              Scaffold.of(context).showSnackBar(snackBar);
            }
          },
        ));
  }

  /// Creates a SnackBar widget ready to be displayed by the Scaffold.
  SnackBar _buildSnackBar({String text}) {
    // Style variables here to improve readability
    final backgroundColor = Theme.of(context).primaryColor;
    final textColor = Theme.of(context).colorScheme.onPrimary;
    final textStyle = Theme.of(context).textTheme.caption.copyWith(color: textColor);

    return SnackBar(
      content: Text(
        text,
        maxLines: 2,
        style: textStyle,
      ),
      duration: const Duration(seconds: 8),
      backgroundColor: backgroundColor,
    );
  }
}