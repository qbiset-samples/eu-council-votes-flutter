/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'package:eu_council_votes/models/country.dart';

/// {@category Model}
/// A class representing a country's vote on a proposal.
class Vote {
  // Properties ---------------------------------------------
  /// The country casting the vote.
  final Country country;

  /// The value of the vote.
  ///
  /// I cannot find a definitive list of possible values, or we could have used an enum for this property.
  final String value;

  // Constructors -------------------------------------------
  /// Default constructor.
  Vote(this.country, this.value);
}
