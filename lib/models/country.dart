/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

/// {@category Model}
/// A class representing one of the countries seating on the European Council.
class Country {
  // Properties ---------------------------------------------
  /// An arbitrary identification number, received from the web API.
  final String identifier;

  /// The name of the country, in English.
  final String name;

  /// The weight of the country's vote.
  ///
  /// This is not used anywhere for the moment but might be useful for a graphical representation of the votes.
  final int voteWeight;

  // Constructors -------------------------------------------
  /// Default constructor.
  Country(this.identifier, this.name, this.voteWeight);

  /// Creates a new instance from an appropriate JSON object.
  factory Country.fromJSON(Map<String, dynamic> json) {
    String identifier = json['country_id'];
    String name = json['country'];
    int voteWeight = int.tryParse(json['vote_weight']);

    return Country(identifier, name, voteWeight);
  }
}