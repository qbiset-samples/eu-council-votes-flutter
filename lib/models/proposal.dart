/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'package:intl/intl.dart';
import 'package:eu_council_votes/models/vote.dart';

/// {@category Model}
/// A class representing one of the proposals the EU Council has voted on.
class Proposal {
  // Class properties ---------------------------------------
  /// Shared date formatter.
  static final DateFormat _dateFormat = DateFormat.yMMMMd();
  
  // Instance properties ------------------------------------
  /// An arbitrary identification number, received from the web API.
  final String identifier;

  /// The full title of the proposal.
  final String title;

  /// The date the proposal was voted on.
  final DateTime date;

  /// A list of [Vote] objects containing the various countries' votes.
  final List<Vote> votes;

  // Computed properties ------------------------------------
  /// Get a String representation of [date], using the shared [_dateFormat].
  String get dateString {
    return Proposal._dateFormat.format(date);
  }

  // Constructors -------------------------------------------
  /// Default constructor.
  Proposal(this.identifier, this.title, this.date, this.votes);
}
