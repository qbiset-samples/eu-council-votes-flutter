/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rxdart/rxdart.dart';

import 'package:eu_council_votes/blocs/proposals_state.dart';
import 'package:eu_council_votes/repositories/proposals_repository.dart';
import 'package:eu_council_votes/ui/pages/proposals_list_page.dart';

/// {@category Bloc}
/// {@subCategory Events}
/// The type of events that will be received by [ProposalsBloc].
enum ProposalsEvent { fetch }

/// {@category Bloc}
/// {@subCategory BLoCs}
/// This BLoC object is in charge of fetching proposals data from the web API
/// and sending it to [ProposalsListPage] along with any other relevant state information.
class ProposalsBloc extends Bloc<ProposalsEvent, ProposalsState> {
  // Properties ---------------------------------------------
  /// The repository used to query the web API.
  final ProposalsRepository _repository = ProposalsRepository();

  // Constructors -------------------------------------------
  /// Default constructor.
  ProposalsBloc() : super(ProposalsInitial());

  // Bloc functions -----------------------------------------
  @override
  Stream<Transition<ProposalsEvent, ProposalsState>> transformEvents(
    Stream<ProposalsEvent> events,
    TransitionFunction<ProposalsEvent, ProposalsState> transitionFn,
  ) {
    return super.transformEvents(
      events.debounceTime(const Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  Stream<ProposalsState> mapEventToState(ProposalsEvent event) async* {
    if (event == ProposalsEvent.fetch) {
      final currentState = state;
      yield ProposalsFetching(proposals: currentState.proposals);
      try {
        final proposals = await _repository.getProposals();
        yield ProposalsSuccess(proposals: proposals);
      } on Exception {
        final errorMessage = _generateErrorMessage(isFirstRequest: currentState.proposals.isEmpty);
        yield ProposalsError(errorMessage, proposals: currentState.proposals);
      }
    }
  }

  // Internal functions -------------------------------------
  /// Returns an appropriate error message depending on whether we are querying the web API
  /// for the first time or trying to refresh data.
  String _generateErrorMessage({@required bool isFirstRequest}) {
    final verb = isFirstRequest ? "download" : "refresh";
    final errorMessage = "We were unable to " +
        verb +
        " the voting records.\nPlease try again later, or contact us if the problem persists.";

    return errorMessage;
  }
}
