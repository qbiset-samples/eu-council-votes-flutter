/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'package:flutter/foundation.dart';
import 'package:equatable/equatable.dart';

import 'package:eu_council_votes/blocs/proposals_bloc.dart';
import 'package:eu_council_votes/models/proposal.dart';
import 'package:eu_council_votes/ui/pages/proposals_list_page.dart';

/// {@category Bloc}
/// {@subCategory States}
/// A state emitted by [ProposalsBloc], used to build the widget tree of [ProposalsListPage].
abstract class ProposalsState extends Equatable {
  // Properties ---------------------------------------------
  /// A list of proposals, sorted by chronological order.
  final List<Proposal> proposals;
  /// Whether the UI should display a refresh indicator or not.
  final bool showRefreshIndicator;

  // Constructors -------------------------------------------
  /// Default constructor.
  ProposalsState(this.proposals, this.showRefreshIndicator);

  // Equatable ----------------------------------------------
  @override
  List<Object> get props => [proposals, showRefreshIndicator];
}

/// {@category Bloc}
/// {@subCategory States}
/// The initial state of [ProposalsBloc], with an empty list of proposals.
class ProposalsInitial extends ProposalsState {
  ProposalsInitial() : super([], false);
}

/// {@category Bloc}
/// {@subCategory States}
/// The state emitted by [ProposalsBloc] when it starts fetching data from the web API.
class ProposalsFetching extends ProposalsState {
  ProposalsFetching({@required List<Proposal> proposals}) : super(proposals, true);
}

/// {@category Bloc}
/// {@subCategory States}
/// The state emitted by [ProposalsBloc] when proposals data has been successfully retrieved.
class ProposalsSuccess extends ProposalsState {
  ProposalsSuccess({@required List<Proposal> proposals}) : super(proposals, false);
}

/// {@category Bloc}
/// {@subCategory States}
/// The state emitted by [ProposalsBloc] when querying the web API failed.
class ProposalsError extends ProposalsState {
  // Properties ---------------------------------------------
  /// An error message that will be displayed in a SnackBar.
  final String errorMessage;

  // Constructors -------------------------------------------
  /// Default constructor.
  ProposalsError(this.errorMessage, {@required List<Proposal> proposals}) : super(proposals, false);

  // Equatable ----------------------------------------------
  @override
  List<Object> get props => [proposals, showRefreshIndicator, errorMessage];
}